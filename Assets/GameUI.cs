﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using EGDEV;

public class GameUI : MonoBehaviour
{
    public IntVariable actionsLeft;
    public TextMeshProUGUI actionsLeftTxt;

    public IntVariable turn;
    public TextMeshProUGUI turnTxt;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        actionsLeftTxt.SetText(actionsLeft.Value.ToString());
        turnTxt.SetText(turn.Value.ToString());
    }
}
