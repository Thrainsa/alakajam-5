﻿using System.Collections;
using System.Collections.Generic;
using EGDEV;
using UnityEngine;

public class Monster : MonoBehaviour
{
    internal Vector3 target;
    public float speed;
    internal MonstersGrid monstersGrid;

    public BooleanVariable gameOver;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (target != null && transform.localPosition != target) {
            float step =  speed * Time.deltaTime;
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, target , step);
            if (Vector3.Distance(transform.localPosition, target) < 0.001f) {
                transform.localPosition = target;
            }
        }
    }

    void OnTriggerEnter2D(Collider2D collision) {
        if (!gameOver.Value && collision.gameObject.GetComponentInParent<Orb>() != null) {
            monstersGrid.Remove(this);
            Destroy(gameObject);
        }
    }
}
