﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EGDEV;

public class MonstersGrid : MonoBehaviour
{
    [Header("Grid parameters")]
    public int width = 6;
    public int height = 10;
    public Vector3 offset;

    [Header("Prefabs")]
    public Monster monsterPrefab;

    public Animator animator;

    private List<Monster> monsters = new List<Monster>();

    public BooleanVariable gameOver;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    internal void Spawn()
    {
        Monster newMonster = Instantiate(monsterPrefab,transform.position, Quaternion.identity, transform);
        bool ok = false;
        while(!ok) {
            ok = true;
            newMonster.transform.localPosition = new Vector3(width+1, Random.Range(0, height));
            monsters.ForEach(monster => {
                if(monster.transform.localPosition == newMonster.transform.localPosition)
                    ok = false;
            });
        }
        newMonster.target = newMonster.transform.localPosition;
        newMonster.monstersGrid = this;

        monsters.Add(newMonster);
    }

    internal void Remove(Monster monster)
    {
        monsters.Remove(monster);
    }

    internal void Move()
    {
        monsters.ForEach(monster => {
            monster.target += Vector3.left;
            if(monster.target.x < 0) {
                gameOver.SetValue(true);
                animator.SetBool("GameOver", true);
            }
        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnDrawGizmosSelected()
    {
        // Display the explosion radius when selected
        Gizmos.color = new Color(1, 1, 0, 1f);
        Vector3 top = Vector3.up * height;
        Vector3 right = Vector3.right * width;
        Gizmos.DrawLine(transform.position - offset, transform.position + top - offset);
        Gizmos.DrawLine(transform.position + right - offset, transform.position + right + top - offset);
    }
}
