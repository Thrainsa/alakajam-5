// ----------------------------------------------------------------------------
// Inspired by Ryan Hipple talk - Unite 2017 - Game Architecture with Scriptable Objects
// ----------------------------------------------------------------------------

using NaughtyAttributes;
using UnityEngine;

namespace EGDEV
{
    [CreateAssetMenu(menuName = "Variable/BooleanVariable")]
    public class BooleanVariable : ScriptableObject
    {
#if UNITY_EDITOR
        [Multiline]
        public string DeveloperDescription = "";
#endif
        public bool resetOnStart;
        [ShowIf("resetOnStart")]
        public bool ResetValue;

        public bool Value;

        public void SetValue(bool value)
        {
            Value = value;
        }

        public void SetValue(BooleanVariable value)
        {
            Value = value.Value;
        }

        public void Toggle()
        {
            Value = !Value;
        }

        private void OnEnable()
        {
            if (resetOnStart)
            {
                hideFlags = HideFlags.DontUnloadUnusedAsset;
                Value = ResetValue;
            }
        }
    }
}