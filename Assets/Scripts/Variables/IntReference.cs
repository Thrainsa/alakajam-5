// ----------------------------------------------------------------------------
// Inspired by Ryan Hipple talk - Unite 2017 - Game Architecture with Scriptable Objects
// ----------------------------------------------------------------------------

using System;

namespace EGDEV
{
    [Serializable]
    public class IntReference
    {
        public bool UseConstant = true;
        public int ConstantValue;
        public IntVariable Variable;

        public IntReference() { }

        public IntReference(int value)
        {
            UseConstant = true;
            ConstantValue = value;
        }

        public int Value
        {
            get { return UseConstant ? ConstantValue : Variable.Value; }
        }

        public static implicit operator int(IntReference reference)
        {
            return reference.Value;
        }
    }
}