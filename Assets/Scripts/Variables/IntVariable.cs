// ----------------------------------------------------------------------------
// Inspired by Ryan Hipple talk - Unite 2017 - Game Architecture with Scriptable Objects
// ----------------------------------------------------------------------------

using NaughtyAttributes;
using UnityEngine;

namespace EGDEV
{
    [CreateAssetMenu(menuName = "Variable/IntVariable")]
    public class IntVariable : ScriptableObject
    {
#if UNITY_EDITOR
        [Multiline]
        public string DeveloperDescription = "";
#endif
        public bool resetOnStart;
        [ShowIf("resetOnStart")]
        public int ResetValue;

        public int Value;

        public void SetValue(int value)
        {
            Value = value;
        }

        public void SetValue(IntVariable value)
        {
            Value = value.Value;
        }

        public void Add(int amount)
        {
            Value += amount;
        }

        public void Add(IntVariable amount)
        {
            Value += amount.Value;
        }

        public void Reset()
        {
            Value = ResetValue;
        }

        private void OnEnable()
        {
            if (resetOnStart)
            {
                hideFlags = HideFlags.DontUnloadUnusedAsset;
                Reset();
            }
        }
    }
}