// ----------------------------------------------------------------------------
// Inspired by Ryan Hipple talk - Unite 2017 - Game Architecture with Scriptable Objects
// ----------------------------------------------------------------------------

using NaughtyAttributes;
using UnityEngine;

namespace EGDEV
{
    [CreateAssetMenu(menuName = "Variable/FloatVariable")]
    public class FloatVariable : ScriptableObject
    {
#if UNITY_EDITOR
        [Multiline]
        public string DeveloperDescription = "";
#endif
        public bool resetOnStart;
        [ShowIf("resetOnStart")]
        public float ResetValue;

        public float Value;

        public void SetValue(float value)
        {
            Value = value;
        }

        public void SetValue(FloatVariable value)
        {
            Value = value.Value;
        }

        public void Add(float amount)
        {
            Value += amount;
        }

        public void Add(FloatVariable amount)
        {
            Value += amount.Value;
        }

        private void OnEnable()
        {
            if (resetOnStart)
            {
                hideFlags = HideFlags.DontUnloadUnusedAsset;
                Value = ResetValue;
            }
        }
    }
}