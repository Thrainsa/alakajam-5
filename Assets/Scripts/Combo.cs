﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Combo
{
    public List<Orb> orbs;
    public Spell spellPrefab;

    private Spell spell;

    public Combo(List<Orb> orbs, Spell spell) {
        this.orbs = orbs;
        this.spellPrefab = spell;
    }

    public void ActivateCombo() {
        foreach (var orb in orbs)
        {
            orb.ActivateCombo();
        }
    }

    public void CastSpell() {
        spell = GameObject.Instantiate(spellPrefab, Vector3.zero, Quaternion.identity);
        spell.Init(orbs);
    }

    public void CleanSpell() {
        GridSystem grid = GameObject.Find("OrbGrid").GetComponent<GridSystem>();
        orbs.ForEach(orb => orb.transform.SetParent(grid.transform));
        GameObject.Destroy(spell.gameObject);
    }

}
