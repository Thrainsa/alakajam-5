﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using EGDEV;

public class GridSystem : MonoBehaviour
{
    [Header("Grid parameters")]
    public int width = 6;
    public int height = 10;
    public int orbsCountByType = 10;

    [Header("Prefabs")]
    public Cell cellPrefab;
    public Orb[] orbPrefabs;

    [Header("Externals data")]
    public BooleanVariable isOrbSelected;
    public IntVariable actionsLeft;
    public Animator animator;

    [Header("Combos")]
    public HorizontalCombo[] horizontalCombos;
    
    public List<Combo> comboReady = new List<Combo>();
    internal Cell[,] cells;
    internal List<Orb> usedOrbs = new List<Orb>();

    internal Orb selectedOrb;

    void Awake()
    {
        cells = new Cell[width, height];
        Cell[] items = GetComponentsInChildren<Cell>();
        for (int x = 0, i = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++, i++)
            {
               cells[x, y] = items[i];
            }
        }
    }

    public void CreateGrid()
    {
        // Clean previous grid
        Cell[] cells = GetComponentsInChildren<Cell>();
        foreach(var cell in cells) {
            DestroyImmediate(cell.gameObject);
        }

        // Create the new one
        for (int x = 0, i = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++, i++)
            {
                CreateCell(x, y, i++);
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < orbsCountByType; i++)
        {
            foreach (var prefab in orbPrefabs)
            {
                Orb orb = Instantiate(prefab) as Orb;
                orb.gameObject.SetActive(false);
                orb.transform.SetParent(transform, false);
                usedOrbs.Add(orb);
            }
        }
        RespawnOrbs();

        usedOrbs.Clear();
        actionsLeft.Reset();
    }

    public void RespawnOrbs()
    {
        if (usedOrbs.Count == 0 || actionsLeft.Value <= 0)
            return;
        for (int i = usedOrbs.Count - 1; i >= 0; i--)
        {
            Orb orb = usedOrbs[i];
            usedOrbs.RemoveAt(i);

            int y = -1;
            while (y == -1)
            {
                y = Random.Range(0, height);
                if (cells[0, y].orb != null)
                    y = -1;
                // TODO Check no spell activated
            }

            AddOrbOnLine(orb, y);

            orb.gameObject.SetActive(true);
        }
        CheckSpells();
        actionsLeft.Add(-1);
        animator.SetInteger("actionsLeft", actionsLeft.Value);
    }

    private void AddOrbOnLine(Orb orb, int y)
    {
        orb.transform.localPosition = new Vector3(-1, y, 0);
        int x = FindNextX(y);
        orb.transform.localPosition = new Vector3(x, y, 0);
        Cell cell = cells[x, y];
        cell.orb = orb;
        orb.cell = cell;
    }

    private void RemoveOrbOnLine(Orb orb)
    {
        Cell initialCell = orb.cell;
        initialCell.SetSelected(false);
        initialCell.orb = null;
        orb.SetSelected(false);

        int y = (int) initialCell.transform.localPosition.y;
        for (int i = width - 1; i >= 0; i--)
        {
            Cell cell = cells[i, y];
            if (i != 0 && cell.orb == null && cells[i - 1, y].orb != null)
            {
                cell.orb = cells[i-1, y].orb;
                cell.orb.transform.localPosition = new Vector3(i, y, 0);
                cell.orb.cell = cell;
                cells[i-1, y].orb = null;
            }
        }
    }

    private int FindNextX(int y)
    {
        for (int i = width - 1; i >= 0; i--)
        {
            if (cells[i, y].orb == null)
            {
                return i;
            }
        }

        return -1;
    }

    // Update is called once per frame
    void Update()
    {
    }

    void OnDrawGizmosSelected()
    {
    }

    void FixedUpdate()
    {
        if(isOrbSelected.Value && selectedOrb != null) {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 10f))
            {
                Transform objectHit = hit.transform;
                PlaceOrbPreviewOnLine(objectHit, selectedOrb);
            }
        }
    }

    private void PlaceOrbPreviewOnLine(Transform cell, Orb orbPreview)
    {
        int y = (int) cell.localPosition.y;
        int x = FindNextX(y);
        if(x < width-1 && cells[x+1, y].orb == orbPreview) {
            x++;
        }
        orbPreview.transform.localPosition = new Vector3(x, y, 0);
    }

    void CreateCell(int x, int y, int i)
    {
        // Cell cell = PrefabUtility.InstantiatePrefab(cellPrefab as Cell) as Cell;
        // cell.transform.SetParent(transform, false);
        // cell.transform.localPosition = new Vector3(x, y, 0);
        // cell.grid = this;
    }

    public void SelectOrb(Orb orb) {
        isOrbSelected.SetValue(true);
        selectedOrb = orb;
        orb.SetSelected(true);
    }

    public void MoveOrb(Cell targetCell) {
        int targetY = (int) targetCell.transform.localPosition.y;
        int previousY = (int) selectedOrb.cell.transform.localPosition.y;
        // Check if we have at least an empty place
        if(targetY != previousY && cells[0, targetY].orb != null) {
            return;
        }
        Cell previousCell = selectedOrb.cell;
        RemoveOrbOnLine(selectedOrb);
        AddOrbOnLine(selectedOrb, (int) targetCell.transform.localPosition.y);

        if (previousCell != selectedOrb.cell)
        {
            CheckSpells();
            actionsLeft.Add(-1);
            animator.SetInteger("actionsLeft", actionsLeft.Value);
        }
        selectedOrb = null;
        isOrbSelected.SetValue(false);
    }

    private void CheckSpells()
    {
        // Check for spells
        foreach (var horizontalCombo in horizontalCombos)
        {
            var result = horizontalCombo.FindCombo(cells);
            foreach (var combo in result)
            {
                combo.ActivateCombo();
                comboReady.Add(combo);
            }
        }
    }

    public void RemoveCombo(Combo combo) {
        for (int i = combo.orbs.Count - 1; i >= 0; i--)
        {
            Orb orb = combo.orbs[i];
            usedOrbs.Add(orb);
            RemoveOrbOnLine(orb);
            orb.gameObject.SetActive(false);
        }
        comboReady.Remove(combo);
    }
}
