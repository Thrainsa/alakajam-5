﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Spell/HorizontalCombo")]
public class HorizontalCombo : ScriptableObject
{
    public int alignCount = 3;
    public Element type;
    public Spell spell;

    public List<Combo> FindCombo(Cell[,] cells)
    {
        List<Combo> result = new List<Combo>();
        for (int y = 0; y < cells.GetLength(1); y++)
        {
            int count = 0;
            List<Orb> comboOrbs = new List<Orb>();
            for (int x = cells.GetLength(0) - 1; x >= 0; x--)
            {
                Cell currentCell = cells[x, y];
                if (currentCell.orb == null)
                {
                    // End of line
                    break;
                }
                if (!currentCell.orb.comboActivated && currentCell.orb.element == type)
                {
                    count++;
                    comboOrbs.Add(currentCell.orb);
                    if (count == alignCount)
                    {
                        result.Add(new Combo(comboOrbs, spell));
                        comboOrbs = new List<Orb>();
                        count = 0;
                    }
                }
                else
                {
                    count = 0;
                    comboOrbs.Clear();
                }
            }
        }
        return result;
    }
}
