﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EGDEV;

public class Cell : MonoBehaviour
{
    public BooleanVariable showGrid;
    public BooleanVariable orbSelected;
    public IntVariable actionsLeft;

    public GameObject background;
    public GameObject selectedBackground;

    public Orb orb;
    
    [HideInInspector]
    public GridSystem grid;

    private bool selected;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(showGrid.Value != background.activeSelf) {
            background.SetActive(showGrid.Value);
        }
    }

    void OnMouseEnter()
    {
        if (!orbSelected.Value && actionsLeft.Value > 0)
            selectedBackground.SetActive(true);
    }

    void OnMouseExit()
    {
        if (!selected)
            selectedBackground.SetActive(false);
    }

    void OnMouseDown() {
        if (!orbSelected.Value && orb != null && !orb.comboActivated && actionsLeft.Value > 0) {
            SetSelected(true);
            grid.SelectOrb(orb);
        } else if (orbSelected.Value) {
            grid.MoveOrb(this);
        }
    }

    public void SetSelected(bool selected) {
        this.selected = selected;
        selectedBackground.SetActive(selected);
    }

}
