using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(GridSystem))]
public class GridEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        
        GridSystem grid = (GridSystem)target;
        if(GUILayout.Button("Build Grid"))
        {
            grid.CreateGrid();
        }
    }
}