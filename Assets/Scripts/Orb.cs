﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orb : MonoBehaviour
{
    public Element element;
    public Material selectedMaterial;
    public GameObject comboBackground;

    internal bool selected;
    internal bool comboActivated;
    internal Cell cell;

    private Material defaultMaterial;

    // Start is called before the first frame update
    void Start()
    {
        defaultMaterial = GetComponent<MeshRenderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    internal void SetSelected(bool selected)
    {
        this.selected = selected;
        if(selected) {
            GetComponent<MeshRenderer>().material = selectedMaterial;
        } else {
            GetComponent<MeshRenderer>().material = defaultMaterial;
        }
    }

    public void ActivateCombo() {
        comboBackground.gameObject.SetActive(true);
        comboActivated = true;
    }

    public void EndCombo() {
        comboBackground.gameObject.SetActive(false);
        comboActivated = false;
    }
}
