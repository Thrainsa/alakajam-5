﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EGDEV;

public class InputManager : MonoBehaviour
{
    public BooleanVariable showGrid;
    public GameEvent respawnOrbs;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("ShowGrid")) {
            showGrid.Toggle();
        }
        if (Input.GetButtonDown("RespawnOrbs")) {
            respawnOrbs.Raise();
        }
    }
}
