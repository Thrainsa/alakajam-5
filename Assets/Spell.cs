﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spell : MonoBehaviour
{
    public List<Orb> orbs;
    public float speed;

    private Transform firstTarget;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void Init(List<Orb> orbs) {
        this.orbs = orbs;
        orbs.ForEach(orb => {
            orb.EndCombo();
            orb.transform.SetParent(transform);
        });
        firstTarget = orbs[0].transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (firstTarget != null) {
            float step =  speed * Time.deltaTime; // calculate distance to move
            for (int i = 1; i < orbs.Count; i++)
            {
                if(orbs[i] != null)
                    orbs[i].transform.position = Vector3.MoveTowards(orbs[i].transform.position, firstTarget.position, step * i);
            }

            // Check if the position of the cube and sphere are approximately equal.
            if (Vector3.Distance(orbs[1].transform.position, firstTarget.position) < 0.001f)
            {
                firstTarget = null;
                for (int i = 1; i < orbs.Count; i++)
                {
                    if(orbs[i] != null)
                        orbs[i].gameObject.SetActive(false);
                }
                // Rotate
                // cast fireball
            }
        } else {
            float step =  speed * Time.deltaTime; // calculate distance to move
            transform.position = Vector3.MoveTowards(transform.position, transform.position + Vector3.right * speed , step * 5);
        }
    }
}
