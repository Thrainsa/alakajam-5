﻿using System.Collections;
using System.Collections.Generic;
using EGDEV;
using UnityEngine;

public class MonsterSpawner : MonoBehaviour
{
    public MonstersGrid monstersGrid;
    public IntVariable turn;

    // Start is called before the first frame update
    void Start()
    {
        monstersGrid.Spawn();
        monstersGrid.Move();
        monstersGrid.Spawn();
        monstersGrid.Move();
        monstersGrid.Spawn();
        monstersGrid.Move();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Spawn() {
        for (int i = 0; i < Mathf.Min(1 + (turn.Value / 5), 4); i++)
        {
            monstersGrid.Spawn();
        }
    }

    public void Move() {
        monstersGrid.Move();
        monstersGrid.Move();
    }
}
