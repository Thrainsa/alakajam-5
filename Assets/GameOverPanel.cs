﻿using System.Collections;
using System.Collections.Generic;
using EGDEV;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverPanel : MonoBehaviour
{
    public IntVariable turn;
    public BooleanVariable gameOver;
    public TextMeshProUGUI turnTxt;

    // Start is called before the first frame update
    void Start()
    {
        turnTxt.SetText(turn.Value.ToString());
    }

    public void Restart() {
        turn.Reset();
        gameOver.SetValue(false);
        SceneManager.LoadScene("main");
    }

    public void Quit() {
        turn.Reset();
        gameOver.SetValue(false);
        Application.Quit();
    }
}
