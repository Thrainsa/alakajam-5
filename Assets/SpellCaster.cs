﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellCaster : MonoBehaviour
{
    public Animator animator;
    public GridSystem grid;

    public void CastSpells() {
        StartCoroutine("CastSpellsCoroutine");
    }

    
    IEnumerator CastSpellsCoroutine() 
    {
        for (int i = grid.comboReady.Count - 1; i >= 0; i--)
        {
            Combo combo = grid.comboReady[i];
            combo.CastSpell();
            yield return new WaitForSeconds(0.25f);
        }
        yield return new WaitForSeconds(1f);
        for (int i = grid.comboReady.Count - 1; i >= 0; i--)
        {
            Combo combo = grid.comboReady[i];
            combo.CleanSpell();
            grid.RemoveCombo(combo);
        }

        animator.SetTrigger("next");
    }
}
